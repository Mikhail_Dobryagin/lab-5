package com.company;

import util.ErrorReturn;
import util.Pair;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Класс, управляющий выполнением комманд
 */
public class Invoker
{
    /**
     * Словарь из комманд -- <имя комманды, <объект комманды, <количество аргументов <i>общее(может быть больше -- тогда в команду нужно передавать список команд)</i>, <количество аргументов <i>для консоли</i>>>>>
     */
    private final HashMap<String, Pair<Command, Pair<Integer, Integer>>> commandMap= new HashMap<>();

    /**
     * Добавить команду <b>без аргументов</b>
     * @param commandName Название команды
     * @param command Объект комманды
     */
    public void register(String commandName, Command command) {
        commandMap.put(commandName, new Pair<>(command,new Pair<>(0,0)));
    }

    /**
     * Добавить команду без аргументов в консоли
     * @param commandName Название команды
     * @param command Объект комманды
     * @param countOfArgs Общее количество аргументов для команды
     */
    public void register(String commandName, Command command, Integer countOfArgs) {
        commandMap.put(commandName, new Pair<>(command, new Pair<>(countOfArgs, 0)));
    }

    /**
     * Добавить команду
     * @param commandName Название команды
     * @param command Объект команды
     * @param countOfArgs Общее количество аргументов для команды (сколько минимум должны подать из списка команд)
     * @param countOfConsoleArgs Количество аргументов, вводимых из терминала
     */
    public void register(String commandName, Command command, Integer countOfArgs, Integer countOfConsoleArgs) {
        commandMap.put(commandName, new Pair<>(command, new Pair<>(countOfArgs, countOfConsoleArgs)));
    }

    /**
     * Исполнить команду, указав аргументы
     * @param commandName Название команды
     * @param args Массив аругментов
     */
    public ErrorReturn execute(String commandName, Object[] args, LinkedList<String> listWithArgs)
    {
        if(!isInInvoker(commandName))
            return new ErrorReturn(2, "Команды \"" + commandName + "\" не существует");

        ErrorReturn error = commandMap.get(commandName).first.execute(args, listWithArgs);
        return error==null ? new ErrorReturn(2, "Неверно введены аргументы") : error;
    }

    /**
     * Исполнить команду, не указывая аргументы
     * @param commandName Название команды
     */
    public ErrorReturn execute(String commandName)
    {
        if(!isInInvoker(commandName))
            return new ErrorReturn(2, "Команды \"" + commandName + "\" не существует");

        ErrorReturn error = commandMap.get(commandName).first.execute();
        return error==null ? new ErrorReturn(2, "Неверно введены аргументы") : error;
    }

    /**
     * Проверить, добавлена ли указанная команда
     * @param commandName Название команды
     */
    public boolean isInInvoker(String commandName)
    {
        return  !(commandMap.get(commandName)==null);
    }

    /**
     * Получить количество аргументов команды
     * @param commandName Название команды
     * @return -1, если такой команды не существует
     */
    public int getCountOfArgs(String commandName)
    {
        if(!isInInvoker(commandName))
            return -1;
        return commandMap.get(commandName).second.first;
    }

    /**
     * Получить количество аргументов команды, вводимых из терминала
     * @param commandName Название команды
     * @return -1, если такой команды не существует
     */
    public int getCountOfConsoleArgs(String commandName)
    {
        if(!isInInvoker(commandName))
            return -1;
        return commandMap.get(commandName).second.second;
    }
}
