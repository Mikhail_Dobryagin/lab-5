package com.company;
import commands.*;
import util.ColAnsi;
import util.ErrorReturn;

import java.util.*;

public class Main
{
    public static void main(String[] args)
    {
        PQofSpacemarines pqs = new PQofSpacemarines();

        LinkedList<String> list = new LinkedList<>();

        {
            ErrorReturn error = (new CommandAddFromXML(pqs)).execute();
            if(error.getCode()!=0)
                System.out.println(ColAnsi.ANSI_RED+error.getStatus()+ColAnsi.ANSI_RESET);
            else
                System.out.println(ColAnsi.ANSI_GREEN + "Коллекция успешно добавлена из файла" + ColAnsi.ANSI_RESET);
        }

        Invoker invoker = new Invoker();

        invoker.register("help", new CommandHelp());
        invoker.register("info", new CommandInfo(pqs));
        invoker.register("show", new CommandShow(pqs));
        invoker.register("add", new CommandAdd(pqs), 8);
        invoker.register("update", new CommandUpdate(pqs), 9, 1);
        invoker.register("remove_by_id", new CommandRemoveById(pqs), 1, 1);
        invoker.register("clear", new CommandClear(pqs));
        invoker.register("save", new CommandSave(pqs));
        invoker.register("execute_script", new CommandExecuteScript(list), 1, 1);
        invoker.register("exit", new CommandExit());
        invoker.register("remove_first", new CommandRemoveFirst(pqs));
        invoker.register("remove_head", new CommandRemoveHead(pqs));
        invoker.register("add_if_min", new CommandAddIfMin(pqs), 8);
        invoker.register("filter_contains_name", new CommandFilterContainsName(pqs), 1, 1);
        invoker.register("filter_starts_with_name",new CommandFilterStartsWithName(pqs), 1, 1);
        invoker.register("print_descending", new CommandPrintDescending(pqs));

        HashSet<String> commandsSet = new HashSet<>();

        while(true)
        {
            if(list.isEmpty())
            {
                commandsSet.clear();
                String[] commandWithArgs;

                try {
                    commandWithArgs = scanCommand();
                }catch (NoSuchElementException | IllegalStateException e)
                {
                    System.out.println(ColAnsi.ANSI_RED + ErrorReturn.endOfScan().getStatus() + ColAnsi.ANSI_RESET);
                    return;
                }


                if (commandWithArgs.length == 1)
                {
                    ErrorReturn error = invoker.execute(commandWithArgs[0]);
                    if (error.getCode() == 1) {
                        System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                        return;
                    } else if (error.getCode() == 0) {
                        if (error.getStatus() != null)
                            System.out.println(ColAnsi.ANSI_GREEN + error.getStatus() + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);
                }
                else
                {
                    if(invoker.getCountOfConsoleArgs(commandWithArgs[0])!=commandWithArgs.length-1)
                    {
                        System.out.println(ColAnsi.ANSI_YELLOW + "Аргументы введены неправильно" + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    ErrorReturn error = invoker.execute(commandWithArgs[0], new Object[]{commandWithArgs[1]}, list);
                    if(error.getCode()==1)
                    {
                        System.out.println();
                        System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                        return;
                    }

                    if(error.getCode()==0)
                    {
                        if (error.getStatus() != null)
                            System.out.println(ColAnsi.ANSI_GREEN + error.getStatus() + ColAnsi.ANSI_RESET);
                    }
                    else
                        System.out.println(ColAnsi.ANSI_YELLOW+error.getStatus()+ColAnsi.ANSI_RESET);

                }

                continue;
            }

            if(list.getFirst().equals("&&&"))
            {
                list.removeFirst();
                continue;
            }

            if(list.getFirst().contains("&&&"))
            {
                System.out.println(ColAnsi.ANSI_RED+"Неверно введены аргументы"+ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            String[] commandNameWithArg = list.getFirst().split(" ", 2);
            list.removeFirst();

            String commandName = commandNameWithArg[0];

            if(!list.isEmpty() && commandName.equals("execute_script"))
            {
                if(commandsSet.contains(list.getFirst())) {
                    System.out.println(ColAnsi.ANSI_RED + "У вас нашли рекурсию\nЗдоровья погибшим" + ColAnsi.ANSI_RESET);
                    list.clear();
                    continue;
                }
                commandsSet.add(list.getFirst());
            }


            int countOfArgs = invoker.getCountOfArgs(commandName);
            int countOfConsoleArgs = invoker.getCountOfConsoleArgs(commandName);

            if(countOfArgs<0)
            {
                System.out.println(ColAnsi.ANSI_RED + "Команды " + commandName + " не существует" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            if(countOfArgs - countOfConsoleArgs > list.size())
            {
                //System.out.println(countOfArgs + "\n" + list.size());
                System.out.println(ColAnsi.ANSI_RED + "Мало аргументов для вызова функции" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            ErrorReturn error=null;

            Object[] argsForCommand = new Object[countOfArgs];
            for(int i=0;i<countOfArgs;i++)
            {
                if(i==0 && commandNameWithArg.length==2)
                    argsForCommand[i]=commandNameWithArg[1];
                else {
                    argsForCommand[i] = list.getFirst();
                    list.removeFirst();
                }

                if(((String)argsForCommand[i]).contains("&&&"))
                {
                    error=new ErrorReturn(2, "Неверно введены аргументы" + "\n" + argsForCommand[i]);
                    break;
                }
            }

            if(error!=null)
            {
                System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            error = countOfArgs == 0 ? invoker.execute(commandName) : invoker.execute(commandName, argsForCommand, list);

            if(error.getCode()==1)
            {
                System.out.println();
                System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                return;
            }

            if(error.getCode()==0)
            {
                if(error.getStatus()!=null)
                    System.out.println(ColAnsi.ANSI_GREEN + error.getStatus() + ColAnsi.ANSI_RESET);
                continue;
            }

            System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);
            list.clear();

        }

    }


    public static String[] scanCommand()
    {
        String[] lines;
        Scanner in = new Scanner(System.in);

        lines = in.nextLine().split(" ", 2);
        return lines;
    }

}
