package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import spacemarine.Chapter;
import spacemarine.SpaceMarine;
import util.FileException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.time.LocalDate;
import java.util.*;

/**
 * Очередь с приоритетом, состоящая из космических десантников, приоритет которой определяется по <b>уровню здоровья</b> десатников
 */
public class PQofSpacemarines {
    private final HashSet<Long> set = new HashSet<>();

    /**
     * @value Переменная окружения
     */
    private final String envVarName="SPACEMARINES";
    /**
     * @value Дата создания структура
     */
    private LocalDate creationDate;
    /**
     * @value Элементы коллекции
     */
    private PriorityQueue<SpaceMarine> pq;
    /**
     * @value Id следующего претендента на добавление в структуру
     */
    private Long commonId;

    public PQofSpacemarines()
    {
        pq = new PriorityQueue<>();
        creationDate = LocalDate.now();
        commonId=1L;
    }

    /** * @exclude */
    public LocalDate getCreationDate()
    {
        return creationDate;
    }
    /** * @exclude */
    public void setCreationDate(LocalDate creationDate)
    {
        this.creationDate = creationDate;
    }
    /** * @exclude */
    public PriorityQueue<SpaceMarine> getPq()
    {
        return pq;
    }
    /** * @exclude */
    public void setPq(PriorityQueue<SpaceMarine> pq)
    {
        this.pq = pq;
    }
    /** * @exclude */
    public Long getCommonId()
    {
        return commonId;
    }
    /** * @exclude */
    public void setCommonId(Long commonId)
    {
        this.commonId = commonId;
    }

    /**
     * Вывести справку по доступным командам
     */
    public static void help()
    {
        System.out.println();
        longLine();
        System.out.println("СПРАВКА ПО КОМАНДАМ");
        System.out.println("____________________");
        System.out.println("\nhelp : вывести справку по доступным командам");
        System.out.println("\ninfo : вывести информацию об отряде десантников (тип, дата создания отряда, количество десантников и т.д.)");
        System.out.println("\nshow : вывести всех десантников в строковом представлении");
        System.out.println("\nadd : добавить нового десантника");
        System.out.println("\tПомимо этого будет предложено ввести следующие поля:");
        System.out.println("\t•Имя\n\t•Координаты\n\t•Здоровье\n\t•Категория\n\t•Оружие\n\t•Вспомогательное оружие\n\t•Имя командира и название отряда");
        System.out.println("\nupdate <id> : обновить десантника, id которой равен заданному");
        System.out.println("\tПомимо этого будет предложено ввести следующие поля:");
        System.out.println("\t•Имя\n\t•Координаты\n\t•Здоровье\n\t•Категория\n\t•Оружие\n\t•Вспомогательное оружие\n\t•Имя командира и название отряда");
        System.out.println("\nremove_by_id <id> : удалить десантника по его id");
        System.out.println("\nclear : очистить отряд десантников");
        System.out.println("\nsave : сохранить отряд десантников в файл");
        System.out.println("\nexecute_script <имя файла> : считать и исполнить набор команд из указанного файла.\nВ нём содержатся команды в таком же виде, в котором их их можно ввести в интерактивном режиме.");
        System.out.println("\nexit : завершить программу (без сохранения в файл)");
        System.out.println("\nremove_first : удалить первого десантника в отряде");
        System.out.println("\nremove_head : вывести первого десантника и удалить его");
        System.out.println("\nadd_if_min : добавить нового десантника в отряд, если его значение меньше, чем у наименьшего десантника в отряде");
        System.out.println("\tПомимо этого будет предложено ввести следующие поля:");
        System.out.println("\t•Имя\n\t•Координаты\n\t•Здоровье\n\t•Категория\n\t•Оружие\n\t•Вспомогательное оружие\n\t•Имя командира и название отряда");
        System.out.println("\nfilter_contains_name <подстрока> : вывести десантников, имена которых содержат заданную подстроку");
        System.out.println("\nfilter_starts_with_name <префикс> : вывести десантников, имена которых начинаются с заданной подстроки");
        System.out.println("\nprint_descending : вывести всех десантников отряда в порядке убывания");
        longLine();
        System.out.println();
    }

    /**
     * Вывести информацию о коллекции
     */
    public void info()
    {
        System.out.println();
        longLine();
        System.out.println("Тип коллекции:        очередь с приоритетом");
        System.out.println("Элементы коллекции:   служащие космического десанта");
        System.out.println("Приоритет:            здоровье десантника");
        System.out.println("Дата инициализации:   " + creationDate);
        System.out.println("Количество элементов: " + pq.size());
        longLine();
        System.out.println();
    }

    public static void longLine()
    {
        System.out.println("________________________________________________________________________________________________________________________");
    }

    /**
     * Вывести все элементы структуры
     */
    public void show()
    {
        Iterator<SpaceMarine> it = pq.iterator();
        System.out.println();
        longLine();
        while (it.hasNext()) {
            it.next().show();
            System.out.println();
        }
        longLine();
        System.out.println();
    }

    /**
     * Добавить элемент в коллекцию
     */
    public boolean add(SpaceMarine o)
    {
        if(o.getId()<=0 || set.contains(o.getId()))
            throw new IllegalArgumentException("Неправильно введён id");
        pq.add(o);
        set.add(o.getId());
        this.commonId=Math.max(this.commonId, o.getId()+1);
        return true;
    }

    /**
     * Удалить элемент из структуры по его Id
     * @return True -- если элемент с таким Id существует
     */
    public boolean remove_by_id(Long id)
    {
        return pq.removeIf(spaceMarine -> spaceMarine.getId().equals(id));
    }

    /**
     * Очистить структуру
     */
    public void clear()
    {
        pq.clear();
    }

    /**
     * Обновить элемент в структуре по его Id
     * @param id Id обновляемого элемента
     * @param newSm Элемент, замещающий элемент по указанному Id
     * @return True -- если существует элемент с данным Id
     */
    public boolean update(Long id, SpaceMarine newSm)
    {
        LocalDate date=null;

        PriorityQueue<SpaceMarine> newPq=new PriorityQueue<>();


        while(pq.size()!=0)
        {
            SpaceMarine curSm = pq.poll();
            if(date==null && curSm.getId().equals(id))
            {
                date=curSm.getCreationDate();
                curSm=newSm;
                curSm.setId(id);
                curSm.setCreationDate(date);
            }
            newPq.add(curSm);
        }
        pq=newPq;

        return date!=null;
    }

    /**
     * Сохранить коллекцию в файл в формате XML
     */
    public void save()
    {
        FileOutputStream file;
        try {
            file = new FileOutputStream(System.getenv(this.envVarName), false);
        }catch (FileNotFoundException e)
        {
            throw new FileException("Не удалось найти файл, заданный переменной окружения (" + this.envVarName + ")");
        }catch (NullPointerException e)
        {
            throw new FileException("Нельзя записать текст в файл, заданный переменной окружения");
        }
        catch(SecurityException e)
        {
            throw new FileException("Запрещён доступ к переменной окружения");
        }

        if(!(new File(System.getenv(envVarName))).canWrite() || (new File(System.getenv(envVarName))).isDirectory() || !(new File(System.getenv(envVarName))).isFile())
            throw new FileException("Нельзя записать текст в файл, заданный переменной окружения");



        try {
            file.write(textForXML().getBytes());
        }catch (IOException e) {
            throw new FileException("Не удалось записать структуру в файл заданный переменной окружения");
        }
    }

    /**
     * Удалить первый элемент из коллекции
     * @return True -- если коллекция не пуста
     */
    public boolean remove_first()
    {
        if(pq.size()>0)
        {
            pq.poll();
            return true;
        }

        return false;
    }

    /**
     * вывести первый элемент коллекции и удалить его
     * @return True -- если структура не пуста
     */
    public boolean remove_head()
    {
        if(pq.isEmpty())
            return false;

        System.out.println();
        longLine();
        pq.poll().show();
        longLine();
        System.out.println();

        return true;
    }

    /**
     * добавить новый элемент в коллекцию, если его значение меньше, чем у наименьшего элемента этой коллекции
     * @return True -- если удалось добавить элемент
     */
    public boolean add_if_min(SpaceMarine newSm)
    {
        for(SpaceMarine curSm : pq)
        {
            if(newSm.compareTo(curSm)>0)
                return false;
            if(newSm.compareTo(curSm)<0)
                continue;

            if(newSm.getCoordinates().compareTo(curSm.getCoordinates())>0)
                return false;
            if(newSm.getCoordinates().compareTo(curSm.getCoordinates())<0)
                continue;

            if(newSm.getId().compareTo(curSm.getId())>=0)
                return false;
        }

        add(newSm);
        return true;
    }

    /**
     * Вывести элементы, значение поля name которых содержит заданную подстроку
     */
    public void filter_contains_name(String s)
    {
        int counter=1;

        for(SpaceMarine sm : pq)
        {
            if(sm.getName().contains(s))
            {
                if(counter==1)
                {
                    System.out.println();
                    longLine();
                }
                System.out.print(counter + ") ");
                counter++;
                sm.show();
            }
        }

        longLine();
        System.out.println("Количество совпадений : " + (counter-1));
        longLine();
        System.out.println();
    }

    /**
     * Вывести элементы, значение поля name которых начинается с заданной подстроки
     */
    public void filter_starts_with_name(String s)
    {
        int counter=1;

        for(SpaceMarine sm : pq)
        {
            if(sm.getName().startsWith(s))
            {
                if(counter==1)
                {
                    System.out.println();
                    longLine();
                }
                System.out.print(counter + ") ");
                counter++;
                sm.show();
            }
        }

        longLine();
        System.out.println("Количество совпадений : " + (counter-1));
        longLine();
        System.out.println();
    }

    /**
     * Вывести элементы коллекции в порядке убывания
     */
    public void print_descending()
    {
        System.out.println();
        longLine();

        SpaceMarine[] spaceMarines = new SpaceMarine[pq.size()];
        {
            int i = 0;
            for (Object sm : pq.toArray())
            {
                spaceMarines[i]=(SpaceMarine)sm;
                i++;
            }
        }

        Arrays.sort(spaceMarines, (o1, o2) -> {
            int result = o1.compareTo(o2);

            if(result!=0)
                return -result;

            result=o1.getCoordinates().compareTo(o2.getCoordinates());
            if(result!=0)
                return -result;

            return -o1.getId().compareTo(o2.getId());
        });

        int len = spaceMarines.length;

        for(int i=0;i<len;i++)
        {
            System.out.print(i+1 + ") ");
            spaceMarines[i].show();
            System.out.println();
        }

        longLine();
        System.out.println();
    }

    /**
     * Достать структуру из XML-файла
     * @param fileName Название файла
     * @return Структура, содержащаяся в файле
     */
    public static PriorityQueue<SpaceMarine> parseXML(String fileName) throws ParserConfigurationException
    {
        fileName=System.getenv(fileName);
        PriorityQueue<SpaceMarine> pq = new PriorityQueue<>();
        File file;

        try {
            file = new File(fileName);
        }catch (NullPointerException e)
        {
            throw new NullPointerException("Файл, заданный перменной окружения не существует");
        }

        if(!file.canRead() || file.isDirectory() || !file.isFile())
            throw new FileException("Файл, заданный перменной окружения нельзя прочитать");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // Получили из фабрики билдер, который парсит XML, создает структуру Document в виде иерархического дерева.
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Запарсили XML, создав структуру Document. Теперь у нас есть доступ ко всем элементам, каким нам нужно.

        Document document;
        try {
            document = builder.parse(file);
        }catch (IOException e)
        {
            throw new FileException("Файл, заданный перменной окружения нельзя прочитать");
        }catch (SAXException e)
        {
            throw new FileException("Файл со структурой записан некорректно");
        }

        NodeList spaceMarines = document.getElementsByTagName("SpaceMarine");

        int len1 = spaceMarines.getLength();

        for(int i=0;i<len1;i++)
        {
            String[] args = new String[SpaceMarine.countOfArgumentsWithoutId+1];
            Node spaceMarineElements = spaceMarines.item(i);
            Element element;
            element = (Element)spaceMarineElements;

            if(
                    element.getElementsByTagName("creationDate").item(0)==null ||
                    element.getElementsByTagName("id").item(0)==null ||
                    element.getElementsByTagName("name").item(0)==null ||
                    ((Element) element.getElementsByTagName("coordinates").item(0)).getElementsByTagName("x")==null ||
                    ((Element) element.getElementsByTagName("coordinates").item(0)).getElementsByTagName("y")==null ||
                    element.getElementsByTagName("health").item(0)==null ||
                    element.getElementsByTagName("category").item(0)==null ||
                    element.getElementsByTagName("weaponType").item(0)==null ||
                    element.getElementsByTagName("meleeWeapon").item(0)==null ||
                    element.getElementsByTagName("chapter").item(0)!=null && ((Element) element.getElementsByTagName("chapter").item(0)).getElementsByTagName("name").item(0)==null
            )
                throw new FileException("Файл со структурой записан некорректно");

            try {
                LocalDate creationDate = LocalDate.parse(element.getElementsByTagName("creationDate").item(0).getTextContent());
                args[0] = element.getElementsByTagName("id").item(0).getTextContent();
                args[1] = element.getElementsByTagName("name").item(0).getTextContent();
                args[2] = ((Element) element.getElementsByTagName("coordinates").item(0)).getElementsByTagName("x").item(0).getTextContent();
                args[3] = ((Element) element.getElementsByTagName("coordinates").item(0)).getElementsByTagName("y").item(0).getTextContent();
                args[4] = element.getElementsByTagName("health").item(0).getTextContent();
                args[5] = element.getElementsByTagName("category").item(0).getTextContent();
                args[6] = element.getElementsByTagName("weaponType").item(0).getTextContent();
                args[7] = element.getElementsByTagName("meleeWeapon").item(0).getTextContent();
                if(element.getElementsByTagName("chapter").item(0)!=null)
                {
                    args[8]="+";
                    args[9] = ((Element) element.getElementsByTagName("chapter").item(0)).getElementsByTagName("name").item(0).getTextContent();
                    if(((Element) element.getElementsByTagName("chapter").item(0)).getElementsByTagName("parentLegion").item(0)!=null)
                        args[10] = ((Element) element.getElementsByTagName("chapter").item(0)).getElementsByTagName("parentLegion").item(0).getTextContent();
                    else
                        args[10]="";
                }
                else
                {
                    args[8]="-";
                    args[9]=null;
                    args[10]=null;
                }

                SpaceMarine newSm = SpaceMarine.newSpaceMarine(args);
                newSm.setCreationDate(creationDate);
                pq.add(newSm);
            }catch (IllegalArgumentException | SpaceMarine.MakeSpacemarineException | Chapter.MakeChapterException e)
            {
                throw new FileException("Файл со структурой записан некорректно" + "\n" + e.getMessage());
            }
        }

        return pq;
    }

    /**
     * Заменить исходную структуру, на структуру, записанную в XML-файле
     */
    public void addFromXML() throws ParserConfigurationException
    {
        PriorityQueue<SpaceMarine> subPq = parseXML(this.envVarName);

        while(!subPq.isEmpty())
            add(subPq.poll());
    }

    /**
     * Составить текст формата XML с исходной структурой
     * @return Файл XML, записанный в строчку
     */
    private String textForXML()
    {

        StringBuilder text = new StringBuilder();

        text.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

        text.append("<PQofSpacemarines>\n");

        PriorityQueue<SpaceMarine> subPq = new PriorityQueue<>();
        while (pq.size()!=0)
        {
            SpaceMarine subSm=pq.poll();
            subPq.add(subSm);
            text.append("    <SpaceMarine>\n");
            text.append("        <id>").append(subSm.getId().toString()).append("</id>\n");
            text.append("        <name>").append(subSm.getName()).append("</name>\n");
            text.append("        <coordinates>\n            <x>").append(subSm.getCoordinates().getX()).append("</x>\n            <y>").append(subSm.getCoordinates().getY()).append("</y>\n        </coordinates>\n");
            text.append("        <creationDate>").append(subSm.getCreationDate().toString()).append("</creationDate>\n");
            text.append("        <health>").append(subSm.getHealth()).append("</health>\n");
            text.append("        <category>").append(subSm.getCategory().toString()).append("</category>\n");
            text.append("        <weaponType>").append(subSm.getWeaponType().toString()).append("</weaponType>\n");
            text.append("        <meleeWeapon>").append(subSm.getMeleeWeapon().toString()).append("</meleeWeapon>\n");
            if(subSm.getChapter()!=null)
            {
                text.append("        <chapter>\n            <name>").append(subSm.getChapter().getName()).append("</name>\n");
                if(subSm.getChapter().getParentLegion()!=null)
                    text.append("            <parentLegion>").append(subSm.getChapter().getParentLegion()).append("</parentLegion>\n");
                text.append("        </chapter>\n");
            }

            text.append("    </SpaceMarine>\n");
        }
        pq=subPq;

        text.append("</PQofSpacemarines>");

        return text.toString();
    }

}
