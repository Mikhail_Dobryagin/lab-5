package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandFilterStartsWithName implements Command
{
    private final PQofSpacemarines pqs;

    public CommandFilterStartsWithName(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public ErrorReturn execute()
    {

        return null;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        pqs.filter_starts_with_name((String)args[0]);
        return ErrorReturn.OK();
    }
}
