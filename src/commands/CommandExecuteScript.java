package commands;

import com.company.Command;
import util.ErrorReturn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CommandExecuteScript implements Command
{
    private final LinkedList<String> list;

    public CommandExecuteScript(LinkedList<String> list)
    {
        this.list = list;
    }

    @Override
    public ErrorReturn execute()
    {
        return null;
    }

    //Просто добавляет в очередь команд новые команды и их аргументы из файла
    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        FileReader fileReader;
        try {
            fileReader = new FileReader((String)args[0]);
        }catch (FileNotFoundException e)
        {
            return new ErrorReturn(2, "Файл не найден");
        }

        StringBuilder stringOfText = new StringBuilder();

        {
            char[] text = new char[1000];
            boolean flag = true;
            if (!(new File((String) args[0])).canRead())
                return new ErrorReturn(2, "Невозможно прочитать файл");

            while(flag)
            {
                flag=false;
                try {
                    if(fileReader.read(text)>=1000)
                        flag=true;
                } catch (IOException e) {
                    return new ErrorReturn(2, "Невозможно прочитать файл");
                }

                try {
                    fileReader.close();
                } catch (IOException e) {
                    return new ErrorReturn(2, "Ошибка при обработке файла");
                }

                for (int i = 0; text[i] != '\0'; i++)
                    stringOfText.append(text[i]);
            }
        }

        String[] lines = stringOfText.toString().replaceAll("[^\\w\\n \\dА-Яа-яёЁ\\\\/.,!@#$%^*()_+~\\-`\\[\\]|><:]", "").split("(?<=[^\\n])\\n");

        ArrayList<String> arrayList = new ArrayList<>();

        for(String line : lines)
            Collections.addAll(arrayList, line);

        int len = arrayList.size();

        for(int index = 0; index < len;index++)
        {
            if(arrayList.get(index).contains("&&&"))
                return new ErrorReturn(2, "Неверно введены аргументы");

            list.add(index, arrayList.get(index));
        }

        list.add(len,"&&&");

        return ErrorReturn.OK();
    }
}
