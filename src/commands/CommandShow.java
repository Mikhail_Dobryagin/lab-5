package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandShow implements Command
{
    private final PQofSpacemarines pqs;
    public CommandShow(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        pqs.show();
        return ErrorReturn.OK();
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }

}
