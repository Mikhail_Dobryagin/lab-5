package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandClear implements Command
{
    private final PQofSpacemarines pqs;
    public CommandClear(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        pqs.clear();
        return new ErrorReturn(0);
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }

}
