package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandPrintDescending implements Command
{
    private final PQofSpacemarines pqs;

    public CommandPrintDescending(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        pqs.print_descending();
        return ErrorReturn.OK();
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }
}
