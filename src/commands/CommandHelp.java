package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandHelp implements Command
{

    @Override
    public ErrorReturn execute()
    {
        PQofSpacemarines.help();
        return ErrorReturn.OK();
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }
}
