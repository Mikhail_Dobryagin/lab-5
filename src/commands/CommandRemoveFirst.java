package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandRemoveFirst implements Command
{
    private final PQofSpacemarines pqs;

    public CommandRemoveFirst(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        return pqs.remove_first() ? new ErrorReturn(0, "Элемент успешно удалён") : new ErrorReturn(2, "Очередь пуста");
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }
}
