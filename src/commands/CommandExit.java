package commands;

import util.ColAnsi;
import com.company.Command;
import util.ErrorReturn;

import java.util.LinkedList;

public class CommandExit implements Command
{
    @Override
    public ErrorReturn execute()
    {
        System.out.println(ColAnsi.ANSI_GREEN + "Пока - пока ☺" + ColAnsi.ANSI_RESET);
        System.exit(0);
        return ErrorReturn.OK();
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }
}
