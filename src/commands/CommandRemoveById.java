package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandRemoveById implements Command
{
    private final PQofSpacemarines pqs;
    public CommandRemoveById(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        return null;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        try {
            return pqs.remove_by_id(Long.parseLong((String) args[0])) ? new ErrorReturn(0, "Элемент успешно удалён") : new ErrorReturn(0, "Элемента с таким id не существует");
        }catch (NumberFormatException e)
        {
            return new ErrorReturn(2, "Ошибка при вводе Id");
        }
    }
}
