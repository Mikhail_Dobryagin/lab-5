package commands;

import com.company.Command;
import spacemarine.Chapter;
import util.ActionWithArg;
import util.EndOfScanException;
import util.ErrorReturn;
import com.company.PQofSpacemarines;
import spacemarine.SpaceMarine;

import java.util.LinkedList;


public class CommandAdd implements Command
{

    private final PQofSpacemarines pqs;

    protected ActionWithArg<String[], ErrorReturn> mainAction = new ActionWithArg<String[], ErrorReturn>()
    {
        @Override
        public ErrorReturn action(String[] args)
        {
            return pqs.add(SpaceMarine.newSpaceMarine(args)) ? new ErrorReturn(0, "Вставка прошла успешно") : new ErrorReturn(0,"Не удалось вставить элемент");
        }
    };

    public CommandAdd(PQofSpacemarines pqs)
    {
        this.pqs=pqs;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> list)
    {

        String[] stringArgs = new String[SpaceMarine.countOfArgumentsWithoutId+1];
        stringArgs[0]=pqs.getCommonId().toString();

        for(int i=1;i<=args.length;i++)
            stringArgs[i]=(String)args[i-1];

        if(args.length==SpaceMarine.countOfArgumentsWithoutId-2 && args[args.length-1].equals("+"))
        {
            if(list.size()<=1)
                return new ErrorReturn(2, "Мало аргументов для вызова функции");

            stringArgs[stringArgs.length-2]=list.getFirst();
            list.removeFirst();
            stringArgs[stringArgs.length-1]= list.getFirst();
            list.removeFirst();

            if(stringArgs[stringArgs.length-2].equals("&&&") || stringArgs[stringArgs.length-1].equals("&&&"))
                return new ErrorReturn(2, "Неверно введены аргументы");
        }

        try{
            return mainAction.action(stringArgs);
        }catch (IllegalArgumentException | Chapter.MakeChapterException | SpaceMarine.MakeSpacemarineException e)
        {
            return new ErrorReturn(2, e.getMessage());
        }
    }

    @Override
    public ErrorReturn execute()
    {
        String[] args = new String[SpaceMarine.countOfArgumentsWithoutId+1]; //...+1(id)
        args[0] = pqs.getCommonId().toString();

        {
            String[] spaceMarineWithoutId;
            try
            {
                spaceMarineWithoutId = SpaceMarine.scanSpaceMarine();
            }catch (EndOfScanException e)
            {
                return ErrorReturn.endOfScan();
            }catch (IllegalArgumentException | SpaceMarine.MakeSpacemarineException | Chapter.MakeChapterException e)
            {
                return new ErrorReturn(2, e.getMessage());
            }

            System.arraycopy(spaceMarineWithoutId, 0, args, 1, SpaceMarine.countOfArgumentsWithoutId);
        }

        try {
            return mainAction.action(args);
        }catch (IllegalArgumentException | SpaceMarine.MakeSpacemarineException | Chapter.MakeChapterException e)
        {
            return new ErrorReturn(2, e.getMessage());
        }

    }
}

