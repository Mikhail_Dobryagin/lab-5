package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;
import util.FileException;

import java.util.LinkedList;

public class CommandSave implements Command
{
    private final PQofSpacemarines pqs;

    public CommandSave(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        try{
            pqs.save();
        }catch (FileException e) {
            return new ErrorReturn(2, e.getMessage());
        }

        return new ErrorReturn(0, "Файл успешно перезаписан");
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }

}
