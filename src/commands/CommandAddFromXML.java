package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;
import util.FileException;

import javax.xml.parsers.ParserConfigurationException;
import java.util.LinkedList;

public class CommandAddFromXML implements Command
{
    private final PQofSpacemarines pqs;

    public CommandAddFromXML(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        try
        {
            pqs.addFromXML();
        }catch (ParserConfigurationException | IllegalArgumentException | NullPointerException | FileException e)
        {
            return new ErrorReturn(2, e.getMessage());
        }

        return new ErrorReturn(0);
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        return null;
    }

}
