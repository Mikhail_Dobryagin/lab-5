package commands;

import util.ErrorReturn;
import com.company.PQofSpacemarines;
import spacemarine.SpaceMarine;

public class CommandAddIfMin extends CommandAdd
{
    public CommandAddIfMin(PQofSpacemarines pqs)
    {
        super(pqs);
        mainAction = args -> pqs.add_if_min(SpaceMarine.newSpaceMarine(args)) ? new ErrorReturn(0, "Вставка прошла успешно") : new ErrorReturn(0,"Не удалось вставить элемент");
    }
}
