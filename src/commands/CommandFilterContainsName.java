package commands;

import com.company.Command;
import util.ErrorReturn;
import com.company.PQofSpacemarines;

import java.util.LinkedList;

public class CommandFilterContainsName implements Command
{
    private final PQofSpacemarines pqs;

    public CommandFilterContainsName(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public ErrorReturn execute()
    {
        return null;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs)
    {
        pqs.filter_contains_name((String)args[0]);
        return ErrorReturn.OK();
    }
}
