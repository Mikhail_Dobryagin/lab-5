package spacemarine;

/**
 * Координаты десантника
 */
public class Coordinates implements Comparable<Coordinates>{
    /**
     * @value <= 594
     */
    private float x; //Максимальное значение поля: 594
    /**
     * @value > -746
     */
    private Long y; //Значение поля должно быть больше -746, Поле не может быть null

    //Getters and setters
    public float getX() {
        return x;
    }
    public void setX(float x) {
        if(!isXCorrect(x))
            throw new IllegalArgumentException("Координата x должна быть не более 594");
        this.x = x;
    }
    public Long getY() {
        return y;
    }
    public void setY(Long y) {
        if(!isYCorrect(y))
            throw new IllegalArgumentException("Координата y должна быть больше -746");
        this.y = y;
    }
    //!Getters and setters

    public static boolean isXCorrect(float x)
    {
        return x<=594.0f;
    }
    public static boolean isYCorrect(Long y)
    {
        return y>-746;
    }

    public Coordinates()
    {

    }
    public Coordinates(float x, Long y) {
        this.setX(x);
        this.setY(y);
    }

    public double radius_vector()
    {
        return Math.hypot(Double.valueOf(getY()), getX());
    }

    /**
     * Метод, сравнивающий 2 координаты
     * Значение координаты для сравнения — длина радиус-вектора в эту точку
     * @param o Сравниваемый объект
     */
    public int compareTo(Coordinates o)
    {
        return Double.compare(radius_vector(), o.radius_vector());
    }
}