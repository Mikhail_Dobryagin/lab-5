package spacemarine;

/**
 * Оружие
 */
public enum Weapon {
    BOLTGUN,
    BOLT_RIFLE,
    PLASMA_GUN,
    COMBI_PLASMA_GUN,
    INFERNO_PISTOL
}
