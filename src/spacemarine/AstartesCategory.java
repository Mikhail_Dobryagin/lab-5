package spacemarine;

/**
 * Класс, обозначающий роль(специальность) десантника
 * @author Михаил
 */
public enum AstartesCategory {
    SCOUT,
    AGGRESSOR,
    TACTICAL,
    TERMINATOR,
    LIBRARIAN
}
