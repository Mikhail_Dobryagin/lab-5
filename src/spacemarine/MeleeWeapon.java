package spacemarine;

/**
 * Дополнительное оружие десантника
 */
public enum MeleeWeapon {
    POWER_SWORD,
    MANREAPER,
    LIGHTING_CLAW,
    POWER_BLADE,
    POWER_FIST
}
