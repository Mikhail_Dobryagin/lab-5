//TODO Сделать ввод нулевого чаптера из файла

package spacemarine;

import util.ColAnsi;
import util.EndOfScanException;

import java.time.*;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Космический десантник
 */
public class SpaceMarine implements Comparable<SpaceMarine>{

    public static final int countOfArgumentsWithoutId = 10; //name, x, y, health, category, weapon, MWeapon, sign, ChapterName, ParentLegion
    public static class MakeSpacemarineException extends RuntimeException
    {
        public MakeSpacemarineException(String message)
        {
            super(message);
        }
    }

    /**
     * @value Поле не может быть null, Значение поля должно быть больше 0, Значение этого поля должно быть уникальным, Значение этого поля должно генерироваться автоматически
     */
    private Long id; //Поле не может быть null, Значение поля должно быть больше 0, Значение этого поля должно быть уникальным, Значение этого поля должно генерироваться автоматически
    /**
     * Имя
     * @value Поле не может быть null, Строка не может быть пустой
     */
    private String name;
    /**
     * Местоположение
     * @value Поле не может быть null
     */
    private Coordinates coordinates;
    /**
     * Дата приступления к службе
     * @value Поле не может быть null, Значение этого поля должно генерироваться автоматически
     */
    private java.time.LocalDate creationDate;
    /**
     * Здоровье
     * @value Значение поля должно быть больше 0
     */
    private double health;
    /**
     * Роль десантника
     * @value Поле не может быть null
     */
    private AstartesCategory category;
    /**
     * Тип оружия
     * @value Поле не может быть null
     */
    private Weapon weaponType;
    /**
     * Тип дополнительного оружия
     * @value Поле не может быть null
     */
    private MeleeWeapon meleeWeapon;
    /**
     * Подразделение
     */
    private Chapter chapter;


    //Getters and setters

    /** * @exclude */
    public Long getId() {
        return id;
    }
    /** * @exclude */
    public void setId(Long id) {
        this.id = id;
    }
    /** * @exclude */
    public String getName() {
        return name;
    }
    /** * @exclude */
    public void setName(String name) {
        if(name.replaceAll("[^\\w[А-Я][а-я]ёЁ\\d ]", "").equals(name))
            this.name = name;
        else
            throw new MakeSpacemarineException("Имя введено некорректно");
    }
    /** * @exclude */
    public Coordinates getCoordinates() {
        return this.coordinates;
    }
    /** * @exclude */
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
    /** * @exclude */
    public java.time.LocalDate getCreationDate() {
        return this.creationDate;
    }
    /** * @exclude */
    public void setCreationDate(java.time.LocalDate creationDate) {
        this.creationDate = creationDate;
    }
    /** * @exclude */
    public double getHealth() {
        return health;
    }
    /** * @exclude */
    public AstartesCategory getCategory() {
        return category;
    }
    /** * @exclude */
    public Weapon getWeaponType() {
        return weaponType;
    }
    /** * @exclude */
    public MeleeWeapon getMeleeWeapon() {
        return meleeWeapon;
    }
    /** * @exclude */
    public void setMeleeWeapon(MeleeWeapon meleeWeapon) {
        this.meleeWeapon = meleeWeapon;
    }
    /** * @exclude */
    public Chapter getChapter() {
        return chapter;
    }
    /** * @exclude */
    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }
    //!Getters and setters

    public SpaceMarine(Long id, String name, Coordinates coordinates, double health, AstartesCategory category, Weapon weaponType, MeleeWeapon meleeWeapon, Chapter chapter) {
        this.id = id;
        this.name = name;
        this.coordinates = coordinates;
        this.setCreationDate(LocalDate.now());
        this.health = health;
        this.category = category;
        this.weaponType = weaponType;
        this.meleeWeapon = meleeWeapon;
        this.chapter = chapter;
    }

    /**
     * Метод создающий нового десантника из переданных текстовых аргументов
     * @param args Id, name, coordinates, health, category, category, weaponType, meleeWeapon, chapter
     * @return Новый десантник
     */
    public static SpaceMarine newSpaceMarine(String[] args)
    {
        long id;
        try{
            id=Long.parseLong(args[0]);
        }catch (NumberFormatException e)
        {
            System.out.println(args[0]);
            throw new NumberFormatException("id введено неправильно");
        }

        if(id<=0)
            throw new MakeSpacemarineException("id введено неправильно");

        String name = args[1];
        if(name.matches(".*[<>\\n\\t].*") || !name.matches(".*[\\wА-Яа-яёЁ\\d].*"))
            throw new MakeSpacemarineException("Имя введено неправильно");

        Coordinates coordinates = new Coordinates();
        coordinates.setX(Float.parseFloat(args[2]));
        coordinates.setY(Long.valueOf(args[3]));
        double health;
        try
        {
            health = Double.parseDouble(args[4]);
        }catch (NumberFormatException e)
        {
            throw new NumberFormatException("Здоровье введено неправильно");
        }

        if(health<=0)
            throw new MakeSpacemarineException("Здоровье введено неправильно");

        AstartesCategory category;
        try
        {
            category = AstartesCategory.valueOf(args[5]);
        }catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Роль ввведена неправильно");
        }
        Weapon weaponType;
        try
        {
            weaponType = Weapon.valueOf(args[6]);
        }catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Такого оружия не существует");
        }

        MeleeWeapon meleeWeapon;
        try {
            meleeWeapon=MeleeWeapon.valueOf(args[7]);
        }catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Такого дополнительного оружия не существует");
        }

        Chapter chapter = Chapter.newChapter(args[8], args[9], args[10]);

        return new SpaceMarine(id, name, coordinates, health, category, weaponType, meleeWeapon, chapter);
    }


    private static <T> T scanField(String inputM, Function<String, T> parser, String parseErrorM, Predicate<T> checker, String checkError)
    {
        T object=null;
        boolean flag=true;

        Scanner in = new Scanner(System.in);

        String str;

        while(flag)
        {
            flag=false;
            System.out.print(inputM);
            try{
                str=in.nextLine();
            }catch (NoSuchElementException | IllegalStateException e)
            {
                throw new EndOfScanException();
            }

            try{
                object=parser.apply(str);
            }catch (IllegalArgumentException | Chapter.MakeChapterException e)
            {
                System.out.println(ColAnsi.ANSI_RED + parseErrorM+ColAnsi.ANSI_RESET);
                flag=true;
                continue;
            }

            if(!checker.test(object))
            {
                System.out.println(ColAnsi.ANSI_RED+checkError + ColAnsi.ANSI_RESET);
                flag=true;
            }

        }

        return object;
    }

    private static String scanName()
    {
        return scanField(
                "Введите имя десантника: ",
                s -> s,
                "",
                s -> !s.matches(".*[<>].*") && s.matches(".*[\\wА-Яа-яёЁ\\d].*"),
                "Имя введено неправильно\n->Должно быть введено имя без странных символов"
        );
    }

    private static Coordinates scanCoordinates()
    {
        Coordinates coordinates = new Coordinates();
        coordinates.setX(
                scanField(
                        "Введите координату x: ",
                        Float::parseFloat,
                        "Координата введена неправильно\n->Должно быть введено дробное число в десятичной записи",
                        Coordinates::isXCorrect,
                        "Координата введена неправильно\n->Координата x должна быть не более 594"
                )
        );

        coordinates.setY(
                scanField(
                        "Введите координату y: ",
                        Long::parseLong,
                        "Координата введена неправильно\n->Должно быть введено целое число в десятичной записи",
                        Coordinates::isYCorrect,
                        "Координата введена неправильно\n->Координата y должна быть больше -746"
                )
        );

        return coordinates;
    }

    private static double scanHealth()
    {
        return scanField(
                "Введите здоровье: ",
                Double::parseDouble,
                "Здоровье введено неправильно\n->Должно быть введено дробное число в десятичной записи",
                x -> x > 0,
                "Здоровье введено неправильно\n->Здоровье должно быть положительным"
        );
    }

    private static AstartesCategory scanCategory()
    {
        return scanField(
                "Введите роль из списка:\n    SCOUT\n" +
                        "    AGGRESSOR\n" +
                        "    TACTICAL\n" +
                        "    TERMINATOR\n" +
                        "    LIBRARIAN\n",
                AstartesCategory::valueOf,
                "Нет такой роли\n->Должна быть выбрана роль из списка",
                astartesCategory -> true,
                ""
        );
    }

    private static Weapon scanWeapon()
    {
        return scanField(
                "Выберите основное оружие из списка:\n    BOLTGUN\n" +
                        "    BOLT_RIFLE\n" +
                        "    PLASMA_GUN\n" +
                        "    COMBI_PLASMA_GUN\n" +
                        "    INFERNO_PISTOL\n",
                Weapon::valueOf,
                "Нет такого оружия\n->Должно быть выбрано оружие из списка",
                weapon -> true,
                ""
        );
    }

    private static MeleeWeapon scanMWeapon()
    {
        return scanField(
                "Выберите дополнительное оружие из списка:\n    POWER_SWORD,\n" +
                        "    MANREAPER\n" +
                        "    LIGHTING_CLAW\n" +
                        "    POWER_BLADE\n" +
                        "    POWER_FIST\n",
                MeleeWeapon::valueOf,
                "Такого дополнительного оружия не существует\n->Дополнительное оружие должно быть выбрано из списка",
                meleeWeapon -> true,
                ""
        );
    }

    private static Chapter scanChapter()
    {
        String hasChapter = null;
        while (hasChapter==null)
        {
            System.out.println("Хотите ли вы ввести подразделение (+/-): ");

            hasChapter = (new Scanner(System.in)).nextLine();

            if (hasChapter.equals("-"))
                return null;

            if(!hasChapter.equals("+"))
            {
                System.out.println(ColAnsi.ANSI_RED + "Введите + или -" + ColAnsi.ANSI_RESET);
                hasChapter=null;
            }
        }

        Chapter chapter=new Chapter();

        chapter.setName(scanField(
                "Введите имя командира: ",
                s -> s,
                "",
                Chapter::isNameCorrect,
                "Имя командира введено неправильно\n->Должно быть введено название поодразделения без странных символов"
        ));

        chapter.setParentLegion(
                scanField(
                        "Введите подразделение: ",
                        Chapter::parseToPL,
                        "Название подразделения введено неверно",
                        Chapter::isPLCorrect,
                        "Название подразделения введено неверно"
                )
        );

        return chapter;
    }

    /**
     * Метод, читающий нового десантника из терминала
     * @return Текстовые аргументы для создания десантника
     */
    public static String[] scanSpaceMarine()
    {
        String name = scanName();
        Coordinates coordinates = scanCoordinates();
        double health = scanHealth();
        AstartesCategory category = scanCategory();
        Weapon weapon = scanWeapon();
        MeleeWeapon meleeWeapon = scanMWeapon();
        Chapter chapter = scanChapter();

        return new String[]{name, String.valueOf(coordinates.getX()), String.valueOf(coordinates.getY()), String.valueOf(health),String.valueOf(category), String.valueOf(weapon),String.valueOf(meleeWeapon), chapter!=null ? "+" : "-", chapter!=null ? chapter.getName() : "", chapter!=null ? chapter.getParentLegion() : ""};
    }

    /**
     * Метод для сравнения десантников по значению их <b>здоровья</b>
     * @param o Десантник для сравнения
     */
    @Override
    public int compareTo(SpaceMarine o)
    {
        return Double.compare(this.getHealth(), o.getHealth());
    }

    @Override
    public String toString()
    {
        return "SpaceMarine{id=" + this.getId() + ",name="+this.getName();
    }

    /**
     * Метод, выводящий  десантника (все его поля и свойства)
     */
    public void show()
    {
        System.out.println("Десантник \"" + this.getName() + "\" :");
        System.out.println("\tid                     | " + this.getId());
        System.out.println("\tЗдоровье               | " + this.getHealth());
        System.out.println("\tКоординаты             | (" + this.getCoordinates().getX() + ", " + this.getCoordinates().getY() + ")");
        System.out.println("\tРоль                   | " + this.getCategory().toString());
        System.out.println("\tДата принятия          | " + this.getCreationDate().toString());
        System.out.println("\tОсновное оружие        | " + this.getWeaponType().toString());
        System.out.println("\tДополнительное оружие  | " + this.getMeleeWeapon().toString());
        Chapter.show(chapter);
    }
}
