package spacemarine;

/**
 * Класс, обозначающий командира и подразделение десатника
 */
public class Chapter {

    public static class MakeChapterException extends RuntimeException
    {
        public MakeChapterException(String message)
        {
            super(message);
        }
    }
    /**
     * @value Имя командира
     */
    private String name; //Поле не может быть null, Строка не может быть пустой
    /**
     * @value Название подразделения
     */
    private String parentLegion;

    //Getters and setters
    /** * @exclude */
    public String getName() {
        return name;
    }

    /** * @exclude */
    public void setName(String name) {
        if(!isNameCorrect(name))
            throw new MakeChapterException("Имя командира должно быть введено без странных символов");
        this.name = name;
    }
    /** * @exclude */
    public String getParentLegion() {
        return parentLegion;
    }
    /** * @exclude */
    public void setParentLegion(String parentLegion) {
        if(!isPLCorrect(parentLegion))
            throw new MakeChapterException("Должно быть введено название поодразделения без странных символов");
        this.parentLegion = parseToPL(parentLegion);
    }
    //!Getters and setters

    public static boolean isNameCorrect(String name)
    {
        return !name.matches(".*[<>\\n\\t].*") && name.matches(".*[\\wА-Яа-яёЁ\\d].*");
    }

    public static boolean isPLCorrect(String parentLegion)
    {
        return !parentLegion.matches(".*[<>].*");
    }

    public static String parseToPL(String s)
    {
        if (!isPLCorrect(s))
            throw new MakeChapterException("Название подразделения введено неверно");

        StringBuilder parentLegion= new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) > 23)
                parentLegion.append(s.charAt(i));
        }

        return parentLegion.toString();
    }

    /**
     * Конструктор
     * @param name Имя командира
     * @param parentLegion Название подразделения
     */
    public Chapter(String name, String parentLegion) {
        setName(name);
        setParentLegion(parseToPL(parentLegion));
    }

    public static Chapter newChapter(String sign, String name, String parentLegion)
    {
        if(sign.equals("-"))
            return null;
        if(!sign.equals("+"))
            throw  new MakeChapterException("Не указано, есть ли глава");
        return new Chapter(name, parentLegion);
    }

    public Chapter()
    {

    }

    public static void show(Chapter chapter)
    {
        if(chapter==null)
            return;
        System.out.println("\tКомандир               | " + chapter.getName());

        if(chapter.getParentLegion().isEmpty())
            return;

        System.out.println("\tПодразделение          | " + chapter.getParentLegion());
    }
}
